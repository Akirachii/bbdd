# Docker Maker v.01

_How to create a database with docker?_

You can clone/download this folder and use it!

``` bash
git clone https://gitlab.com/Akirachii/bbdd.git

or

wget -r https://gitlab.com/Akirachii/bbdd/-/tree/main/dockers?ref_type=heads
```

## Usage

There will be two needed files and one optional;

- docker-compose.yml
- .env
- [opcional] script.txt

You will also be able to change the config from file "__.env__" like this;

```code
    MYSQL_HOST={DesiredName}
    MYSQL_ROOT_PASSWORD={DesiredPasswordForRoot}
    MYSQL_ROOT=root
    MYSQL_USER={DesiredUser}
    MYSQL_PASSWORD={DesiredPasswordForUser}
    MYSQL_DATABASE={LEAVE EMPTY}
```
And change the desired conf of "__docker-compose.yml__" like so:

_The desired conf to change is;_

```code
        ports:
                - 33006:3306
```
_Also;_

```code
      networks:
        - {DesiredNetName}

networks:
  edu-shared:
    external: true
```

 ### __Keep in mind__

For this to work, __the selected ports__ must be not used and the NET must be created before generating the docker, wich bring us to....

_Usage of the "script.txt"_

```bash
docker network create {DesiredNetName} (Must be the same as in the config file)
docker-compose up --build or docker-compose up -d
```

## How to comunicate docker with Workbench

Since I'm usually a common user due to my late neuron activation (lilte meme :D), We'll supose i forgot how to connect or maybe the software isnt responding. Said software in this case is _MYSQL Workbench_.

First steps:

1. You should have the docker started....Now, How do i know if it is started or not?

```docker
docker ps -a
```
This command will show us how many "containers" are availabe and we simply must seek for the {DesiredName} and copy its ID number

```docker
docker start {IDnumber}
```

2. Once the docker is up, start _Workbench_ and select the "+" button on the left and will create a new connection with all the desired info matching the "__.env__" file.

1. You should have the connection established now!! And if you dont.. gl <3.