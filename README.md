# <span style="color:pink;">Pau's DB repositories  </span>

## This is basic documentation over DB module,

Explanation of the database structure, schema, and how to interact with it.

Want to skip and just see the deployment? here's the usage and sql scripts [Test&Deploy](#test-and-deploy)!

Also want to see some DB explanations and overall documentation? (Non related to this repository) [SeeRecap](#see-the-recap)


## __The repository is organized as follows:__
```
📦 bbdd   
┣ 📃 README  
┣ 📂 Dockers  
┃ ┣ 📃 README   
┃ ┣ 📂 DockerMaker   
┃ ┃ ┣ 📃 README 
┃ ┃ ┣ 📃 Config files      
┃ ┃ ┗         
┃ ┣   [/////]  
┃ ┗     
┣ 📂 Scripts
┃ ┣ 📂 Instagram-queries-1  
┃ ┣ 📂 UD-C1-[1 to 3]  
┃ ┣    [/////]       
┃ ┗      
┣ 📂 Complements
┃ ┣ 📃 RECAP    
┃ ┣ 🆘 HELPS       
┃ ┗        
┗  
```

<img src="./Complements/dbimage.png" alt="db example" width="100%" />

## __See the RECAP__

- [] [Lets see the recap](https://gitlab.com/Akirachii/bbdd/-/blob/main/Complements/RECAP.md?ref_type=heads)

## __I work with some classmates to understand everything;__

- [] [See Carlos's GitLab~](https://gitlab.com/Xolrak)
- [] [See Alvaro's GitLab~](https://gitlab.com/alvaro05p)

## __Test and Deploy__

Peek at the scripts and usage folder to see the designated tasks and management of the db.

- [ ] [See the complete folder](https://gitlab.com/Akirachii/bbdd/-/tree/main/scripts?ref_type=heads)


## __Authors and acknowledgment__
Show your appreciation to those who have contributed to the project.