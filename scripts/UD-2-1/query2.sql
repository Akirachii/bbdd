SELECT * FROM Chinook.Invoice AS I
WHERE YEAR(I.InvoiceDate) = YEAR(NOW())
AND MONTH(I.InvoiceDate) BETWEEN 1 AND 3;
