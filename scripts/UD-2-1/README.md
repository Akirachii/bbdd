# UD-2-1 Queries with aggregation functions

By Pau Gradoli,

Today we'll work over some queries as to get used to bigger databases.


## 1st. Find all France clients. 

To find all clients we'll select everything from the table Customers ```SELECT * FROM Chinook.Customer AS C``` and state the alias as _"C"_ and then select where the column "Country" matches "France" ```WHERE C.Country = "France";```

```SQL
SELECT * FROM Chinook.Customer AS C
WHERE C.Country = "France";
```

## 2nd. Shows invoices for the first quarter of this year.  

```SQL
SELECT * FROM Chinook.Invoice AS I
WHERE YEAR(I.InvoiceDate) = YEAR(NOW())
AND MONTH(I.InvoiceDate) BETWEEN 1 AND 3;
```

## 3rd. Shows all songs composed by AC/DC

```SQL
SELECT * FROM Chinook.Track AS T
WHERE T.Composer = "AC/DC"
```

## 4th. Shows the 10 songs that occupy the largest size.

```SQL
SELECT * FROM Chinook.Track AS T
WHERE T.Bytes > 0 
ORDER BY T.Bytes DESC
LIMIT 10
```

## 5th. Shows the name of those countries in which we have clients

```SQL
SELECT DISTINCT Country FROM Chinook.Customer AS C
```

## 6th. Shows all musical genres.

```SQL
SELECT * FROM Chinook.Genre
```

## 7th. Shows all artists along with their albums.

```SQL
SELECT T.Name,T.Composer AS N FROM Chinook.Track AS T
JOIN Artist A ON T.Composer LIKE A.Name
```

## 8th. Shows the names of the 15 youngest employees next to the names of their supervisors, if they have them.

```SQL

```



https://github.com/lerocha/chinook-database/releases/download/v1.4.5/Chinook_MySql.sql