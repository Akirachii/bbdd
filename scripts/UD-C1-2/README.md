# UD-C1-2 'Empresa' & 'Videoclub' DB Inquiries

Pau Gradoli

We will work with the _'Empresa' & 'Videoclub'_ databases. These are the steps;

## Query 1 from _Empresa_ 

### List all products from the company database.

- This query retrieves all columns from the "PRODUCTE" table, which likely contains information about products in the company's inventory.
- Alternatively, it selects specific columns, such as product number and description, for a more concise output.

```SQL
SELECT * FROM PRODUCTE; 

OR

SELECT PROD_NUM, DESCRIPCIO FROM PRODUCTE;
```

## Query 2 from _Empresa_

### List products with 'TENNIS' in their description.

- This query filters products based on their description using the LIKE operator with a pattern match '%TENNIS%'.
- It retrieves the product number and description of products containing the term 'TENNIS' in their description.

```SQL
SELECT PROD_NUM, DESCRIPCIO FROM PRODUCTE
WHERE DESCRIPCIO LIKE '%TENNIS%';
```

## Query 3 from _Empresa_

### List client details including their code, name, area, and telephone number.

- This query selects specific columns from the "CLIENT" table, including the client code, name, area, and telephone number.

```SQL
SELECT CLIENT_COD AS COD, NOM, AREA, TELEFON FROM CLIENT;
```

## Query 4 from _Empresa_

### List clients excluding those with area code '636' at the beginning.

- This query filters clients based on their area code using the SUBSTRING function to extract the first three characters of the area code.
- It retrieves client details excluding those with an area code starting with '636'.


```SQL
SELECT CLIENT_COD AS COD, NOM, CIUTAT,AREA,TELEFON FROM CLIENT
WHERE SUBSTRING(AREA,1,3) != '636';
```

## Query 5 from _Empresa_

### List order numbers along with order and shipment dates from the orders table.

- This query selects order numbers, order dates, and shipment dates from the "COMANDA" table, which likely contains information about orders placed and shipped.


```SQL
SELECT COM_NUM AS CODE, COM_DATA AS FECHA_DE_ORDEN, DATA_TRAMESA AS FECHA_DE_ENVIO FROM COMANDA
```

## Query 6 from _Videoclub_

### List client names and telephone numbers from the video store database.

- This query retrieves client names and telephone numbers from the "CLIENT" table in the video store database.

```SQL
SELECT Nom, Telefon FROM CLIENT
```

## Query 7 from _Videoclub_

### List invoice dates and amounts from the invoices table.

- This query selects invoice dates and amounts from the "FACTURA" table, likely containing information about invoices issued by the video store.

```SQL
SELECT Data, Import FROM FACTURA
```

## Query 8 from _Videoclub_

### List descriptions of items on a specific invoice (invoice number '3').

- This query retrieves descriptions of items from the "DETALLFACTURA" table for a specific invoice with the invoice number '3'.

```SQL
SELECT Descripcio FROM DETALLFACTURA
WHERE CodiFactura = '3';
```

## Query 9 from _Videoclub_

### List all items from invoices in descending order of unit price.

- This query retrieves all columns from the "DETALLFACTURA" table and orders the results by unit price in descending order.

```SQL
SELECT * FROM DETALLFACTURA
ORDER BY PreuUnitari DESC;
```

## Query 10 from _Videoclub_

### List all actors whose names start with 'X'.

- This query selects all columns from the "ACTOR" table where the actor's name starts with 'X', using the LIKE operator with the pattern 'X%'.

```SQL
SELECT * FROM ACTOR
WHERE Nom LIKE 'X%'
```