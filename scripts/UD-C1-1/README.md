# UD-C1 Health DB Inquiries

Pau Gradoli

We will work with the sanitat _"Health"_ database, provided in the sanitat.sql file. These are the steps;

## Query1 

__Objective:__ Retrieve hospital number, name, and phone number from the database.  
Using the query, we'll ask for table "HOSPITAL" and demand the columns HOSPITAL_COD, NOM, TELEFON.

```SQL
SELECT HOSPITAL_COD,NOM,TELEFON FROM HOSPITAL;
```

## Query2

__Objective:__ Retrieve hospitals number, name, and phone number, which contain an _"A"_ as the second letter of the name from the database.  
Using the query, we'll ask for table "HOSPITAL" and demand the columns HOSPITAL_COD, NOM, TELEFON __ONLY__ if the second letter is an "A".

```SQL
SELECT HOSPITAL_COD,NOM,TELEFON FROM HOSPITAL
WHERE SUBSTRING(NOM, 2, 1) = 'A';
```

## Query3

__Objective:__ Retrieve HOSPITAL_COD, SALA_COD, EMPLEAT_NO,COGNOM, from the database.  
Using the query, we'll ask for table "PLANTILLA" and demand the columns HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM.

```SQL
SELECT HOSPITAL_COD,SALA_COD,EMPLEAT_NO,COGNOM FROM PLANTILLA;
```

## Query4

__Objective:__ Retrieve HOSPITAL_COD, SALA_COD, EMPLEAT_NO,COGNOM, if the worker has a Nightshift from the database.  
Using the query, we'll ask for table "PLANTILLA" and demand the columns HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM, And select where the column TORN has an N(Nightshift).

```SQL
SELECT HOSPITAL_COD,SALA_COD,EMPLEAT_NO,COGNOM FROM PLANTILLA
WHERE TORN = 'N';
```

## Query5

__Objective:__ Retrieve all sickmen born 1960 from the database.  
Using the query, we'll ask for table "MALALT" and demand the columns DATA_NAIX, COGNOM. And select where the string of "DATA_NAIX" has as the position 1 (of 4 digits) a match with "1960".

```SQL
SELECT DATA_NAIX, COGNOM FROM MALALT
WHERE substring(DATA_NAIX,1,4) = 1960;
```

## Query6

__Objective:__ Retrieve all sickmen born after 1960 from the database.  
Using the query, we'll ask for table "MALALT" and demand the columns DATA_NAIX, COGNOM. And select where the date is higher than '1960-01-01'.

```SQL
SELECT DATA_NAIX, COGNOM FROM MALALT
WHERE DATA_NAIX > '1960-01-01';
```