-- Photos of the user with ID 36 taken in January 2023
SELECT url FROM fotos
WHERE idUsuario = 36
AND YEAR(fechaCreacion) = 2023
AND MONTH(fechaCreacion) = 1;