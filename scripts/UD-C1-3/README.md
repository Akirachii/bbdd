# UD-C1-3 'Videoclub' DB Inquiries

Pau Gradoli

We will work with the _'Videoclub'_ database. As for putting in practice the __JOIN__ explained last class.


## Query 1 from _Videoclub_ 

### List all movies from the video store along with their genre names.'

- We're selecting the movie title (aliased as "P") from the "PELICULA" table and the genre description (aliased as "G") from the "GENERE" table.
- We're using the JOIN keyword to combine rows from both tables based on a related column, which in this case is PELICULA.CodiPeli from the "PELICULA" table and ID from the "GENERE" table.

```SQL
SELECT PELICULA.Titol AS P, GENERE.Descripcio AS G FROM PELICULA
JOIN GENERE ON PELICULA.CodiPeli = GENERE.CodiGenere;
```

## Query 2 from _Videoclub_ 

### List all invoices from Maria:

- We use the LIKE operator instead of the = operator to perform a pattern match on the "Nom" column of the "CLIENT" table.
- The pattern 'Maria%' matches any name in the "CLIENT" table that starts with 'Maria'.
- This ensures that we find all invoices associated with clients whose names start with 'Maria'.
  
```SQL
SELECT * FROM FACTURA
WHERE DNI IN (SELECT DNI FROM CLIENT WHERE Nom LIKE 'Maria%');
```

## Query 3 from _Videoclub_ 

### List movies along with their main actor.

- Select the movie title from the "PELICULA" table, aliasing it as "TP", and the actor's name from the "ACTOR" table, aliasing it as "AP". 
- The main table from which data will be retrieved is the "PELICULA" table. The alias "P" is used to refer to this table in the query
- Perform a join operation between the "PELICULA" table (aliased as "P") and the "ACTOR" table (aliased as "A"). It connects rows from both tables based on the "CodiActor" column, which presumably contains the ID of the main actor for each movie.

```SQL
SELECT P.Titol AS TP, A.Nom AS AP FROM PELICULA P
JOIN ACTOR A ON P.CodiActor = A.CodiActor;
```

## Query 4 from _Videoclub_ 

### List movies along with all the actors who portrayed them.

- We select the movie title from the "PELICULA" table, aliasing it as "TP", and the name of the actor from the "ACTOR" table, aliasing it as "NA".
- We use the JOIN keyword to combine rows from the "PELICULA", "INTERPRETADA", and "ACTOR" tables based on the IDs of the movies and actors in the "INTERPRETADA" table.
- The first JOIN links the "PELICULA" and "INTERPRETADA" tables based on the movie ID.
- The second JOIN links the "INTERPRETADA" and "ACTOR" tables based on the actor ID.

```SQL
SELECT P.Titol AS TP, A.Nom AS NA FROM PELICULA P
JOIN INTERPRETADA I ON P.CodiPeli = I.CodiPeli
JOIN ACTOR A ON I.CodiActor = A.CodiActor;
```

## Query 5 from _Videoclub_ 

### List IDs and names of movies along with the IDs and names of their sequels.

-  The query selects specific columns from the "PELICULA" table and aliases them for clarity.
- Specify the main table from which data will be retrieved, and aliases the "PELICULA" table as P1.
- The LEFT JOIN ensures that all rows from the left table (P1) are included in the result set, even if there are no matching rows in the right table (P2). If a movie does not have a sequel, the columns from the P2 table will contain NULL values.

```SQL
SELECT P1.CodiPeli AS IDP, P1.Titol AS TP, P2.CodiPeli AS SECUE, P2.Titol AS SECUET FROM PELICULA P1
LEFT JOIN PELICULA P2 ON P1.SegonaPart = P2.CodiPeli;
```

#### Buuuuuut, and if you wanted to display all the movies that dont have sequels??? Just so you dont have to see the "null"?

- Skippinf the explanation of the aliases and select..
- The INNER JOIN will combine rows from the "PELICULA" table (aliased as P1) with rows from itself (aliased as P2), based on the condition P1.SegonaPart = P2.CodiPeli.
- This condition matches original movies with their sequels by comparing the "SegonaPart" column of the first table (P1) with the "CodiPeli" column of the second table (P2), which presumably contains the ID of the sequel movie.

```SQL
SELECT P1.CodiPeli AS DP, P1.Titol AS TP, P2.CodiPeli AS IDSEC, P2.Titol AS SECTITL FROM PELICULA P1
INNER JOIN PELICULA P2 ON P1.SegonaPart = P2.CodiPeli;
```
