SELECT P.Titol AS TP, A.Nom AS NA FROM PELICULA P
JOIN INTERPRETADA I ON P.CodiPeli = I.CodiPeli
JOIN ACTOR A ON I.CodiActor = A.CodiActor;
