# Some helps and usual commands

Sometimes we forget what we even got for breakfast so this will surely help myself and you out, these are commands and little helps to rhelp you remember how to do basics in SQL.

| Commands | What they do? | 
|------|---------------|
| DROP SCHEMA ${NAME-SCHEMA} CASCADE; |  This statement is used to drop a schema along with all of its contained objects. Without CASCADE, you would need to drop each object individually before dropping the schema itself.  |
| ORDER BY ${NAME-ROW} [ASC / DESC] | This query will return the data from ${NAME-COLUMN} ordered (as stated). |
| LIMIT ${NUM-ROWS}; | This clause is used to limit the number of rows returned by the query. | 
| SELECT DISTINCT ${NAME-COLUMN} | This clause is used to select unique values from one or more columns in the result set. |
| WHERE ${NAME-ROW} IN (1, 2, 3) | This query will return all rows where the ${NAME-COLUMN} matches any of the values 1, 2, or 3. |
| WHERE condition1 OR condition2 OR | This query will return x if condition1 is complete or condition2 if it matches this one |



### By Pau :D Have fun!