# C0 : RECAP

By Pau <3.

## Concept and Origin of Databases

_What is a database?_  

A database is a __software system__ that organizes and manages data in a structured format. It allows __efficient storage__, retrieval, and manipulation of data, often using __keys__ to establish relationships between different pieces of information.

_Origin of databases_

In the 1960s, computer applications operated in batch mode, handling specific tasks with limited data types. Each application used transaction files to update or query one or two master files. As technology advanced, programs started allowing multiple users to access files online. This led to the need for online updates and the integration of applications, requiring the elimination of redundancy in data storage. New file structures were designed for interconnectivity, consolidating redundant information. Fast-access physical structures like indexes and hashing techniques were developed for efficient online access and data management.

_What are they trying to solve?_  

They try and fix some problems related to data storage, organization, and retrieval;

- Data redundancy. ~ [ ]
- Data inconsistency. ~ [ ]
- Difficulty of access. ~ [ ]
- Lack of security. ~ [ ]
- Difficulty of sharing data. ~ [ ]

## Database Management Systems (DBMS)
_What is a Database Management Systems (DBMS)?_  

Is a __software application__ to store, organize, and manage data in a database. It offers a systematic approach to data handling, ensuring efficient access, retrieval, and manipulation of information.

_What data access features should a database management system (DBMS) provide?_

- Query. ~ [The DBMS should provide a language for querying the database, such as SQL]
- Data modification. ~ [The DBMS should allow users to add, modify, and delete data in the database. ]
- Data security. ~ [The DBMS should protect the data from unauthorized access, modification, or destruction. ]
- Data concurrency. ~ [The DBMS should allow multiple users to access the database simultaneously without compromising the integrity of the data.]
- Data recovery. ~ [The DBMS should provide mechanisms for recovering the database in case of a failure.]
- Data performance. ~ [The DBMS should optimize the performance of data access operations.]
- Data scalability. ~ [The DBMS should be able to scale to meet the growing needs of the organization.]
- Data integration. ~ [The DBMS should be able to integrate with other systems, such as enterprise resource planning (ERP) systems and customer relationship management (CRM) systems.]
- Data usability. ~ [The DBMS should provide tools and features that make it easy for users to access and use the data.]

###  Examples of database management systems (DBMSs):

| DBMS | Free software | Client-Server |
|------|---------------|---------------|
| Oracle DB | Theres both paid and free options | Yes |
| IMB Db2 | Theres both paid and free options | Yes |
| SQLite | Yes | No |
| MariaDB | Yes | Yes |
| SQL Server | Yes | Yes |
| PostgreSQL | Yes | Yes |
| mySQL | Theres both paid and free options | Yes, but has more to it |

## Client-server model
<img src="./Client-server-model.png" alt="db example" width="100%" />

_Why is it interesting that the DBMS is located on a server?_   

- Centralized data storage  
- Improved performance
- Increased security  
- Enhanced scalability
- Reduced costs  

_What are the advantages of decoupling the DBMS from the client?_   

It easier to __scale__ the application, clients can switch between different DBMS vendors without rewriting code, updates become independent, clients can access the __same data__ through standardized interfaces, can __optimize queries__ and data access.

_What is the client-server model based on?_

The client initiates the communication by sending a request to the server. The server then processes the request and sends a response back to the client.

* __Client__: Any program or device that initiates requests to a server for resources or services.
* __Server__: A computer program or device that provides resources or services to other programs or devices, also known as clients.
* __Net__: Allows it to communicate with other devices and share resources.
* __Ports__: These are virtual communication channels within the server's software, identified by unique numbers.
* __Request__: Refers to an instruction or message sent by a client to the server. 
* __Response__: The information or action sent back by the server in reply to a client's request. 

## SQL
_What is SQL?_  

Stands for __(Structured Query Language)__, is a domain-specific programming language designed specifically for managing data, especially in relational databases.  

_What kind of languaje is it?_  

So, while SQL doesn't fit neatly into any single category, it can be best described as a domain-specific, declarative, set-based query language. 

### SQL Instrucctions.

#### DDL (Data Definition Language)

##### CREATE

Here we will create the table in the selected schema [if none selected, write "USE ${NAME OF DB}"], also will indicate the row ID (as a primary key), a name(always filled and never to be set null or 0) and mail (last one shuld be unique).

```SQL
CREATE TABLE users (
  id INT PRIMARY KEY,
  Name VARCHAR(255) NOT NULL,
  Mail VARCHAR(255) UNIQUE
);
``` 
##### ALTER

Here we are to modify the already created table "users" and add the row age (set as numeric).

```SQL
ALTER TABLE users ADD age INT;
```
##### DROP

And we also want to "_unalive_" the table; 

```SQL
DROP TABLE users;
```

#### DML (Data Manipulation Language)

##### SELECT

Now we want to get some info from the table, here we'll see everything (*) from table "USERS" where the "state" matches "1".

```SQL
SELECT * FROM USERS WHERE state = '1';
```

##### INSERT INFO

In this case only if we want to add info into said rows. ({NAME}, {MAIL}, {CITY}) as stated before "VALUES"

```SQL
INSERT INTO users (name, Mail, city) VALUES ('John Doe', 'john.doe@example.com', 'New York');
```

##### UPDATE

Here, we will change the info in "MAIL" to the selected one.

```SQL
UPDATE users SET Mail = 'new_email@example.com' WHERE id = 1;
```

##### DELETE FROM

And finally here to delete the last order (comparing to the date of today).

```SQL
DELETE FROM customers WHERE last_order_date < DATE_SUB(CURDATE(), INTERVAL 1 YEAR);
```

#### DCL (Data Control Language)

##### GRANT

```SQL
GRANT SELECT ON customers TO analyst;
```
##### REVOKE

```SQL
REVOKE UPDATE ON orders FROM sales;
```
##### DENY

```SQL
DENY DROP ON * TO guest;
```

#### TCL (Transaction Control Language)

##### BEGIN TRANSACTION

##### COMMIT

##### ROLLBACK

##### SAVEPOINT


## Relational databases
_What is a relational database?_   

A relational database is a type of database that stores data in tables. Each table has a set of columns, and each column contains a specific type of data, such as names, dates, or numbers.

_What advantages has to it?_ 

Relational databases remain a prevalent and versatile choice for many organizations due to their strong combination of data organization, querying capabilities, data integrity, and scalability.

_What elements build them?_  

| Elements | What do they do |
|------|---------------|  
| Tables | The core element of a relational database, where data is stored in a structured format. |
| Schema | Defines the overall structure of the database, outlining the tables, their relationships, and the data types of each column. |
| Columns | Represent individual characteristics or properties of the entities stored in the table. |
| Rows | Represent individual records or instances of the entity represented by the table. |
| Keys | Establishing relationships between tables and ensuring data integrity. |
| Data Types | Define the format and range of values that can be stored in each column. |
| Constraints | Rules that further enforce data integrity by restricting the values that can be stored in columns or tables. |

_What are these?_

* __Relation (table)__: It serves as the container for your data, similar to a spreadsheet with rows and columns.
* __Attribute/Field (Attribute)__:  Individual characteristics or properties of the entities stored in a table.
* __Register/Tuple (Row)__:  Each row in a table captures a specific instance of the entity the table represents, along with the values for all its attributes (columns).


## Relations and Bio

- _Information about the Client-Server architecture is based on what i researched form [Red-Switches](https://www.redswitches.com/blog/client-server-architecture/),_

- _Information about the origin of db is from the pdf handled in class [PDF](https://www.uoc.edu/pdf/masters/oficiales/img/913.pdf),_





_Thanks to @guyikcgg for corrections and hints on the work. [See His Profile](https://gitlab.com/guyikcgg)_
